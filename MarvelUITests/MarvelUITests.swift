//
//  MarvelUITests.swift
//  MarvelUITests
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import XCTest

class MarvelUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

}
