//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import XCTest
@testable import Marvel


class MarvelTests: XCTestCase {

    let characterModelView = CharacterViewModel()
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
        
        let mock = CharacterMock()
        let character = mock.characterDataMock()
        
        let modelView = CharacterViewModel()
        //Desfavoritar o Personagem que favoritamos
        modelView.favoriteCharacter(character: character, isFavorited: false)
        
    }

    func testCharactersList() {
        
        let expectationAPIs = expectation(description: "Aguardar retornar os Personagens")
        
        let mock = CharacterMock()
        characterModelView.delegate = mock
        
        //Realiza a busca dos personagens
        characterModelView.loadCharacters()
        
        mock.charactersClosure = {characters in
            //Número de Personagens que é retornado na primeira requisição
            XCTAssertEqual(characters.count, 20)
            
            expectationAPIs.fulfill()
        }
        
        waitForExpectations(timeout: 3.0)
        
    }
    
    func testFavoriteCharacter() {
        
        let expectationAPIs = expectation(description: "Aguardar retornar os Personagens")
        
        let mock = CharacterMock()
        characterModelView.delegate = mock
        
        let character = mock.characterDataMock()
        
        //O personagem deve estar desfavoritado, acabei de criar o objeto
        var isFavorited = characterModelView.isFavoritedCharacter(characterID: character.id)
        XCTAssertFalse(isFavorited)
        
        //Checamos se o delegate é chamado quando o personagem é favoritado e se o ID corresponde ao mesmo personagem
        mock.characterFavoritedChangedClosure = {characterMock in
            
            //Personagem alterado deve ter o mesmo ID do Personagem favoritado
            XCTAssertEqual(characterMock.id, character.id)
            
            //O personagem deve estar favoritado
            isFavorited = self.characterModelView.isFavoritedCharacter(characterID: character.id)
            XCTAssertTrue(isFavorited)
            
            expectationAPIs.fulfill()
            
        }
        
        //Favorito o personagem
        characterModelView.favoriteCharacter(character: character, isFavorited: true)
        
        waitForExpectations(timeout: 1.0)
        
    }
    
    

}

