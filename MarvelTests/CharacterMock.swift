//
//  CharacterMock.swift
//  MarvelTests
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

class CharacterMock: CharacterDelegate {
    
    var charactersClosure: (([Character]) -> Void)?
    var characterFavoritedChangedClosure: ((Character) -> Void)?
    
    func characterListLoaded(characters: [Character]) {
        charactersClosure!(characters)
    }
    
    func characterFavoritedChanged(character: Character) {
        characterFavoritedChangedClosure!(character)
    }
    
    func characterDataMock() -> Character {
        let id:Int = 1000
        let name:String = "Personagem Marvel"
        let description:String = "Personagem ficticio para testarmos"
        let thumbnail = Thumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784", extension:
            "jpg")
        let character = Character(id: id, name: name, description: description, thumbnail: thumbnail)
        return character
    }
    
}
