//
//  Colors.swift
//  Marvel
//
//  Created by Renato Cardial on 8/22/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

struct Colors {
    static let primary    = "EC1D24"
    static let secondary  = "151515"
    static let background  = "F7F7F9"
    static let textPrimary  = "FFFFFF"
}
