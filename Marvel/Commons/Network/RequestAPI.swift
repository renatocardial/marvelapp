//
//  RequestAPI.swift
//  Marvel
//
//  Created by Renato Cardial on 8/22/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

public protocol RequestAPI: Encodable {
    associatedtype Response: Decodable
}
