//
//  File.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import Foundation

class NetworkService {
    
    static var instance:NetworkService?
    
    enum Method:String {
        case DELETE = "DELETE",
        HEAD = "HEAD",
        GET = "GET",
        POST = "POST",
        PUT = "PUT"
    }
    
    static func getInstance() -> NetworkService {
        if self.instance == nil {
            self.instance = NetworkService()
        }
        return self.instance!
    }
    
    func request<T:RequestAPI>(model:T, endpoint: String, method: Method, params:String = "", completion:@escaping (_ success:Bool, _ result: AnyObject) -> Void){
        
        var endpointPath = endpoint
        if(method == .GET){
            if(endpointPath.contains("?")){
                endpointPath = endpointPath + "&"
            }else{
                endpointPath = endpointPath + "?"
            }
        }
        
        let url = URL(string: Environment.API.endPoint(name: endpoint))
        var request = URLRequest(url: url!)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //Na documentação da API Marvel diz que é possível melhorar a performance da resposta da API, comprimindo com GZIP, para isso é necessário colocar esse parâmetro (mais info: https://developer.marvel.com/documentation/generalinfo)
        request.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        
        if(method == .POST){
            let postString = params
            if(!postString.isEmpty){
                request.httpBody = postString.data(using: .utf8)
            }
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.async {
                    completion(false,error!.localizedDescription as AnyObject)
                }
            } else {
                if let usableData = data {
                
                    if let responseObj = try? JSONDecoder.init().decode(ResponseAPI<T.Response>.self, from: usableData) {
                        
                        if responseObj.code == 200 {
                            completion(true, responseObj.data?.results as AnyObject)
                        }else{
                            completion(false,responseObj.message as AnyObject)
                        }
                        
                    } else {
                        completion(false, "Erro ao realizar o Parsing".localized() as AnyObject)
                    }
                }
            }
        }
        task.resume()
        
    }
    
    private func serialize<T>(model:T.Type,json:AnyObject) -> Decodable?{
        do{
            let jsonData: Data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            let modelDecode = model as? Decodable.Type
            let decoded = try? modelDecode!.init(jsonData: jsonData)
            return decoded
        }catch let error{
            print(error)
        }
        return nil
    }
}
