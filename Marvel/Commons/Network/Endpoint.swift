//
//  Endpoint.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

enum Endpoint:String {
    case Characters = "v1/public/characters"
    case Character = "v1/public/characters/@id"
    
    func param(key:String, value:String) -> String{
        return self.rawValue.replacingOccurrences(of: key, with: value)
    }
    
}
