//
//  ResponseAPI.swift
//  Marvel
//
//  Created by Renato Cardial on 8/22/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

struct ResponseAPI<Model:Decodable>:Decodable{
    public let code: Int
    public let status: String?
    public let message: String?
    public let data: DataResponseAPI<Model>?
}

struct DataResponseAPI<Results:Decodable>:Decodable{
    public let offset: Int
    public let limit: Int
    public let total: Int
    public let count: Int
    public let results: Results
    
}
