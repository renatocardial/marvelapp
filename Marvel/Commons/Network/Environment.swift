//
//  Environment.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//
import Foundation
struct Environment {
    
    struct API {
        static let baseUrl = "https://gateway.marvel.com/"
        static let publicKey = "f49c3da2933e5b3b17d96ca511088ad7"
        static let privateKey = "1a1778caa76f356686a382dede061f0e8b71ba86"
        static func endPoint(name : String) -> String{
            let ts = NSDate().timeIntervalSince1970
            let hash = "\(ts)\(privateKey)\(publicKey)".md5
            return "\(Environment.API.baseUrl + name)?apikey=\(publicKey)&ts=\(ts)&hash=\(hash)"
        }
    }
    
}
