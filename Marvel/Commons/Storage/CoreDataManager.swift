//
//  CoreDataManager.swift
//  Marvel
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import CoreData

class CoreDataManager{
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Marvel")
        container.loadPersistentStores(completionHandler: { (description, error) in
            if let error = error {
                fatalError("Unresolved error \(error), \(error.localizedDescription)")
            }
        })
        
        return container
    }()
    
    public var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
}
