//
//  File.swift
//  Marvel
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//
import Foundation

class File {
    
    class func documentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectory = paths[0] as String
        return documentDirectory as NSString
    }
    
    class func saveData(resultToSave: Data, path:String){
        let file = documentsDirectory().appendingPathComponent(path.replacingOccurrences(of: "/", with: "_"))
        NSKeyedArchiver.archiveRootObject(resultToSave, toFile: file)
    }
    
    class func clearData(path:String){
        let file = documentsDirectory().appendingPathComponent(path.replacingOccurrences(of: "/", with: "_"))
        do {
            if FileManager.default.fileExists(atPath: file) {
                try FileManager.default.removeItem(atPath:  file)
            }
        } catch let error {
            print(error)
        }
    }
    
    class func loadData(path: String) -> Data? {
        let file = documentsDirectory().appendingPathComponent(path.replacingOccurrences(of: "/", with: "_"))
        let result = NSKeyedUnarchiver.unarchiveObject(withFile: file)
        return result as? Data
    }
}
