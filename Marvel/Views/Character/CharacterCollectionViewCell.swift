//
//  CharacterCollectionViewCell.swift
//  Marvel
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import UIKit

class CharacterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var thumbImageView: UIImageView!
    
    func configure(character: Character) {
        
        nameLabel.text = character.name
        thumbImageView.load(url: URL(string: character.thumbnail.thumbPath())!)
        
    }
    
}
