//
//  CharacterDetailViewController.swift
//  Marvel
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import UIKit
import CoreData

class CharacterDetailViewController: UIViewController {

    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var thumbImageView: UIImageView!
    @IBOutlet fileprivate weak var descriptionTextView: UITextView!
    @IBOutlet fileprivate weak var favoriteButton: UIBarButtonItem!
    
    var character:Character!
    var characterViewModel = CharacterViewModel()
    var isFavorite:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        loadData()
    }
    
    func setupViews() {
        view.backgroundColor =  UIColor(hexString: Colors.background)
        descriptionTextView.backgroundColor = UIColor.clear
        favoriteButton.tag = characterViewModel.isFavoritedCharacter(characterID: character.id) ? 1 : 0
        changeIconFavorite()
        if isFavorite {
            favoriteButton.accessibilityLabel = "\(character.name) \("é um personagem favorito".localized())"
        }else{
            favoriteButton.accessibilityLabel = "\(character.name) \("não é um personagem favorito".localized())"
        }
    }
    
    func loadData(){
        nameLabel.text = character.name
        nameLabel.accessibilityLabel = character.name
        
        thumbImageView.load(url: URL(string: character.thumbnail.thumbPath())!)
        
        descriptionTextView.text = character.description.isEmpty ? "Não há informações sobre este personagem".localized() : character.description
        descriptionTextView.accessibilityLabel = descriptionTextView.text!
        
    }
    
    func changeIconFavorite(){
        isFavorite = (favoriteButton.tag == 1)
        favoriteButton.image = isFavorite ?  UIImage(named: "favorited") : UIImage(named: "unfavorited")
    }
    
    
    @IBAction func favoriteOrNo(){
        favoriteButton.tag = favoriteButton.tag == 0 ? 1 : 0
        changeIconFavorite()
        characterViewModel.favoriteCharacter(character: character, isFavorited: isFavorite)
    }
    
}
