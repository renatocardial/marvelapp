//
//  FavoritedCharacterViewController.swift
//  Marvel
//
//  Created by Renato Cardial on 8/23/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import UIKit

class FavoritedCharacterViewController: UIViewController {
    
    @IBOutlet var loadingView: UIActivityIndicatorView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    var characterViewModel = CharacterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadData()
    }
    
    func setupViews() {
        
        characterViewModel.delegate = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        loadingView.hidesWhenStopped = false
        loadingView.startAnimating()
        
        messageLabel.isHidden = true
        messageLabel.text = "Nenhum personagem favoritado".localized()
        
        loadingView.color = UIColor.init(hexString: Colors.primary)
        
        collectionView.register(UINib(nibName: "CharacterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CharacterCollectionViewCell")
        collectionView.backgroundColor = UIColor.clear
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            let size = CGSize(width:(collectionView!.bounds.width-20)/2, height: 260)
            layout.estimatedItemSize = size
        }
        
        view.backgroundColor =  UIColor(hexString: Colors.background)
        
    }
    
    func loadData() {
        loadingView.isHidden = false
        characterViewModel.loadFavoritedCharacters()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "FavoritedCharacterDetail"){
            let index = (sender as! Int)
            let vc = segue.destination as! CharacterDetailViewController
            vc.character = self.characterViewModel.characters[index]
            vc.characterViewModel.delegate = self
        }
    }

}

extension FavoritedCharacterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characterViewModel.characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterCollectionViewCell", for: indexPath) as! CharacterCollectionViewCell
        
        let character = self.characterViewModel.characters[indexPath.row]
        cell.configure(character: character)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "FavoritedCharacterDetail", sender: indexPath.row)
    }
}

extension FavoritedCharacterViewController:CharacterDelegate {
   
    func characterListLoaded(characters: [Character]) {
        loadingView.isHidden = true
        messageLabel.isHidden = !(characters.count == 0)
        collectionView.reloadData()
    }
    
    func characterFavoritedChanged(character: Character) {
        loadData()
    }
    
}
