//
//  CharacterViewController.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import UIKit

class CharacterViewController: UIViewController {
    
    @IBOutlet var loadingView: UIActivityIndicatorView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var reloadButton: UIButton!
    
    var characterViewModel:CharacterViewModel = CharacterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupViews()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Tenta carregar novamente caso não tenha personagens
        if characterViewModel.characters.count == 0 && messageLabel.isHidden == false {
            loadData()
        }
    }
    
    func setupViews() {
        characterViewModel.delegate = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.accessibilityIdentifier = "CharacterCollection"
        
        loadingView.hidesWhenStopped = false
        loadingView.startAnimating()
        
        messageLabel.isHidden = true
        messageLabel.text = "Nenhum personagem localizado".localized()
        messageLabel.accessibilityLabel = messageLabel.text
        
        reloadButton.isHidden = messageLabel.isHidden
        reloadButton.backgroundColor = UIColor.init(hexString: Colors.primary)
        reloadButton.titleLabel?.textColor = UIColor.init(hexString: Colors.textPrimary)
        
        loadingView.color = UIColor.init(hexString: Colors.primary)
   
        collectionView.register(UINib(nibName: "CharacterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CharacterCollectionViewCell")
        collectionView.backgroundColor = UIColor.clear
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            let size = CGSize(width:(collectionView!.bounds.width-20)/2, height: 260)
            layout.estimatedItemSize = size
        }
        
        view.backgroundColor =  UIColor(hexString: Colors.background)
        
    }
    
    func loadData() {
        loadingView.isHidden = false
        characterViewModel.loadCharacters()
    }
    
    @IBAction func showFavorites(){
        self.performSegue(withIdentifier: "FavoritedCharacter", sender: nil)
    }
    
    @IBAction func realodData(){
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CharacterDetail"){
            let index = (sender as! Int)
            let vc = segue.destination as! CharacterDetailViewController
            vc.character = self.characterViewModel.characters[index]
        }
    }

}

extension CharacterViewController: CharacterDelegate {

    func characterListLoaded(characters: [Character]) {
        
        loadingView.isHidden = true
        messageLabel.isHidden = !(characters.count == 0)
        reloadButton.isHidden = messageLabel.isHidden
        collectionView.reloadData()
        
    }
    
    func characterFavoritedChanged(character: Character) { }
}


extension CharacterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characterViewModel.characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterCollectionViewCell", for: indexPath) as! CharacterCollectionViewCell
        
        let character = self.characterViewModel.characters[indexPath.row]
        cell.configure(character: character)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "CharacterDetail", sender: indexPath.row)
    }
}
