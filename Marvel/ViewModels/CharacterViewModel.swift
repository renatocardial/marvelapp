//
//  CharacterViewModel.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

import UIKit
import CoreData

enum CharacterError: Error {
    case Default(String)
}

protocol CharacterDelegate {
    func characterListLoaded(characters:[Character])
    func characterFavoritedChanged(character:Character)
}

class CharacterViewModel {

    var delegate:CharacterDelegate?
    var service:CharacterService = CharacterService()
    var characters:[Character] = []
    var objectContext:NSManagedObjectContext?
    
    func getManagedContext() throws -> NSManagedObjectContext {
        if objectContext == nil {
            let coreDataManager = CoreDataManager()
            objectContext = coreDataManager.context
        }
        return objectContext!
    }
    
    func loadCharacters(){
        service.characterList { (characters) in
            self.characters = characters!
            DispatchQueue.main.async {
                self.delegate?.characterListLoaded(characters: self.characters)
            }
        }
    }
    
    func loadFavoritedCharacters(){
        
        do{
            let managedContext = try getManagedContext()
            
            let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: "CharacterEntity")
            let objects = try! managedContext.fetch(fetchRequest)
            var characters:[Character] = []
            for object in objects {
                if let object = object as? NSManagedObject {
                    
                    let id:Int = object.value(forKey: "id") as! Int
                    let name:String = object.value(forKey: "name") as! String
                    let description:String = object.value(forKey: "detail") as! String
                    let thumbPath:String = object.value(forKey: "thumbPath") as! String
                    let pathExtension = (thumbPath as NSString).pathExtension
                    let path = thumbPath.replacingOccurrences(of: ".\(pathExtension)", with: "")
                    let thumbnail = Thumbnail(path: path, extension: pathExtension)
                    
                    let character = Character(id: id, name: name, description: description, thumbnail: thumbnail)
                    
                    characters.append(character)
                }
            }
            DispatchQueue.main.async {
                self.characters = characters
                self.delegate?.characterListLoaded(characters: characters)
            }
        }catch let error as NSError {
            print("Could not load. \(error), \(error.userInfo)")
        }
        
    }
    
    func favoriteCharacter(character:Character, isFavorited:Bool) {
        
        do {
            let managedContext = try getManagedContext()
            let entity = NSEntityDescription.entity(forEntityName: "CharacterEntity",
                                                    in: managedContext)!
            
            if isFavorited {
                let characterObj = NSManagedObject(entity: entity,
                                                   insertInto: managedContext)
                characterObj.setValue(character.id, forKeyPath: "id")
                characterObj.setValue(character.name, forKeyPath: "name")
                characterObj.setValue(character.description, forKeyPath: "detail")
                characterObj.setValue(character.thumbnail.thumbPath(), forKeyPath: "thumbPath")
                
            }else{
                let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: "CharacterEntity")
                fetchRequest.predicate = NSPredicate(format: "id = %d",character.id)
                let object = try! managedContext.fetch(fetchRequest)
                if object.count > 0 {
                    let obj = object[0] as! NSManagedObject
                    managedContext.delete(obj)
                }
            }
            
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        if delegate != nil {
            delegate?.characterFavoritedChanged(character: character)
        }
    }
    
    func isFavoritedCharacter(characterID:Int) -> Bool {
        do{
            let managedContext = try getManagedContext()
            let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: "CharacterEntity")
            fetchRequest.predicate = NSPredicate(format: "id = %d",characterID)
            let objects = try! managedContext.fetch(fetchRequest)
            return objects.count > 0
        }catch let error as NSError {
            print("Could not load. \(error), \(error.userInfo)")
            return false
        }
    }
    
}
