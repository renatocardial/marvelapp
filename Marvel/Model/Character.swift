//
//  Character.swift
//  Marvel
//
//  Created by Renato Cardial on 8/21/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

struct Character:Decodable {
    let id:Int
    let name:String
    let description:String
    let thumbnail:Thumbnail
}
