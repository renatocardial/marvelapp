//
//  Thumbnail.swift
//  Marvel
//
//  Created by Renato Cardial on 8/22/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

struct Thumbnail:Decodable {
    let path:String
    let `extension`:String
    
    func thumbPath() -> String{
        //Necessário modificar a url para https por questão de segurança
        let httpsPath = path.replacingOccurrences(of: "http://", with: "https://")
        return "\(httpsPath).\(self.extension)"
    }
}
