//
//  CharacterService.swift
//  Marvel
//
//  Created by Renato Cardial on 8/22/19.
//  Copyright © 2019 Renato Cardial. All rights reserved.
//

struct CharacterService:RequestAPI {
    
    public typealias Response = [Character]
    
    public func characterList(completion: @escaping ([Character]?) -> ()) {
        
        NetworkService.getInstance().request(model: self, endpoint: Endpoint.Characters.rawValue, method: .GET) { (success, characters) in
            
            completion( success ? characters as! [Character] : [])
            
        }
    }
    
    public func characterById(id:String, completion: @escaping (Character?) -> ()) {
        
        NetworkService.getInstance().request(model: self, endpoint: Endpoint.Character.param(key: "@id", value: id), method: .GET) { (success, characters) in
            
            if success {
                if let characters = characters as? [Character] {
                    completion(characters[0])
                    return
                }
            }
            
            completion(nil)
            
        }
    }
}
